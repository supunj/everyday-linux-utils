#!/bin/bash

# Author - Supun Jayathilake (supunj@gmail.com)

if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
  echo "Power is on"
  sudo shutdown -c
else
  echo "On UPS...shutdown after 1 minute"
  sudo shutdown -P +1
fi